from django.shortcuts import render, get_object_or_404, reverse
from django.http import HttpResponse, HttpResponseRedirect
from .models import Questions, Choice
from django.views import generic

class IndexView(generic.ListView):
    template_name='polls/index.html'
    context_object_name='latest_question_list'

    def get_queryset(self):
        return Questions.objects.order_by('-pub_date')[:5]

class DetailView(generic.DetailView):
    model=Questions
    template_name='polls/detail.html'
    context_object_name='question'

class ResultsView(generic.DetailView):
    model=Questions
    template_name='polls/results.html'
    context_object_name='question'
# Create your views here.
# def index(request):
#     latest_question_list=Questions.objects.order_by('-pub_date')
#     output=','.join([q.question_text for q in latest_question_list])
#     return render(request,'polls/index.html',{'latest_question_list':latest_question_list})
#
# def detail(request, question_id):
#     q=get_object_or_404(Questions,pk=question_id)
#     return render(request,'polls/detail.html',{'question':q})
#
# def results(request, question_id):
#     question = get_object_or_404(Questions,pk=question_id)
#     return render(request, 'polls/results.html',{'question':question})

def vote(request,question_id):
    question = get_object_or_404(Questions,pk=question_id)
    try:
        selected_choice=question.choice_set.get(pk=request.POST['choice'])
    except(KeyError,Choice.DoesNotExist):
        return render(request,'polls/detail.html',{'question':question,'error_message':'Please select a choice first.'})
    else:
        selected_choice.votes+=1
        selected_choice.save()
        return HttpResponseRedirect(reverse("polls:results",args=(question_id,)))