from django.contrib import admin
from .models import Questions, Choice
# Register your models here.
#admin.site.register(Questions)

class QuestionsAdm(admin.ModelAdmin):
    fieldsets=[
        ('Publication Date',{'fields':['question_text']}),
        ('Date Information',{'fields':['pub_date']}),
    ]

admin.site.register(Questions,QuestionsAdm)
admin.site.register(Choice)