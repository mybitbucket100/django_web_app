from django.db import models
from datetime import datetime
# Create your models here.
class Questions(models.Model):
    question_text = models.CharField('Actual Question', max_length=200)
    pub_date = models.DateTimeField('date published')

    # def __repr__(self):
    #     return f"Questions(question_text='{self.question_text}',pub_date='{self.pub_date}')"

    def __str__(self):
        return self.question_text

class Choice(models.Model):
    question = models.ForeignKey(Questions, on_delete=models.CASCADE)
    choice = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice